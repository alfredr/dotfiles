set nocompatible
filetype off

set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()

Plugin 'gmarik/Vundle.vim'
Plugin 'scrooloose/nerdtree'
Plugin 'bling/vim-airline'
Plugin 'kien/ctrlp.vim'
Plugin 'mhinz/vim-signify'
Plugin 'tpope/vim-fugitive'

call vundle#end()
filetype plugin indent on

set ts=4
set sw=4
set smarttab
set expandtab
set nowrap
set mouse=a
set number
colorscheme jellybeans
set t_Co=256
syntax on

set laststatus=2
map <C-n> :NERDTreeToggle<CR>
map <C-f> :Fmt<CR>

set mouse+=a
if &term =~ '^screen'
  set ttymouse=xterm2
endif

